"""
Exit node selection panel.
"""

import random
import sys
import time

import gobject
import gtk

from util import gtkTools, log, torTools, uiTools
from TorCtl import TorCtl

class ExitNodePanel:
  def __init__(self, builder):
    self.builder = builder

  def pack_widgets(self):
    listStore = self.builder.get_object('liststore_countries')

    checkButton = self.builder.get_object('checkbutton_exitnode')
    checkButton.connect('toggled', self.on_checkbutton_exitnode_toggled)

    addButton = self.builder.get_object('button_add_country')
    addButton.connect('clicked', self.on_add_button_clicked, listStore)

    removeButton = self.builder.get_object('button_remove_country')
    treeView = self.builder.get_object('treeview_countries')
    treeSelection = treeView.get_selection()

    removeButton.connect('clicked', self.on_remove_button_clicked)
    treeSelection.connect('changed', self.on_treeselection_countries_cursor_changed)

  def on_add_button_clicked(self, addButton, listStore):
    country = gtkTools.input_country('Select a country to add')

    if country:
      listStore.append((country.code, country.name))

    self.update_exit_nodes()

  def on_remove_button_clicked(self, removeButton):
    treeView = self.builder.get_object('treeview_countries')
    treeSelection = treeView.get_selection()
    model, treeIter = treeSelection.get_selected()

    if treeIter:
      model.remove(treeIter)

    self.update_exit_nodes()

  def on_checkbutton_exitnode_toggled(self, checkButton, data=None):
    active = checkButton.get_active()
    conn = torTools.getConn()

    if active:
      conn.setOption('StrictExitNodes', '1')
    else:
      conn.setOption('StrictExitNodes', '0')

    for name in ('treeview_countries', 'button_add_country'):
      widget = self.builder.get_object(name)
      widget.set_sensitive(active)

  def on_treeselection_countries_cursor_changed(self, treeSelection):
    model, treeIter = treeSelection.get_selected()
    removeButton = self.builder.get_object('button_remove_country')
    removeButton.set_sensitive(bool(treeIter))

  def update_exit_nodes(self):
    listStore = self.builder.get_object('liststore_countries')

    codes = []
    for row in listStore:
      codes.append("{%s}" % row[0])

    torTools.getConn().setOption('ExitNodes', ",".join(codes))

