"""
Provides a wizard for selecting exit node.
"""

import os
import sys
import curses

import cli.controller

from cli import popups
from util import conf, country, log, panel, torTools, uiTools

def showExitNodeDialog():
  selectedCountry = promptCountry()
  conn = torTools.getConn()

  if selectedCountry.code:
    conn.setOption('StrictExitNodes', '1')
    conn.setOption('ExitNodes', '{%s}' % selectedCountry.code)

    log.log(log.NOTICE, "%s {%s} selected for exit nodes" % (selectedCountry.name, selectedCountry.code))
  elif selectedCountry.name == "Disable":
    conn.setOption('StrictExitNodes', '0')
    conn.setOption('ExitNodes', '')

    log.log(log.NOTICE, "Exit node selection disabled")
  elif selectedCountry.name == "Cancel":
    pass

def promptCountry(initialSelection=None):
  countries = country.get_exit_countries()
  countryLines = ["Disable"]
  countryLines += [c.name for c in countries]

  conn = torTools.getConn()
  oldCountry, oldSelection = conn.getOption('ExitNodes')[1:3], -1
  for c in countries:
    if c.code == oldCountry:
      oldSelection = countryLines.index(c.name)

  selectionIndex = popups.showMenu("Please select a country", countryLines, oldSelection)
  if selectionIndex == -1:
    return country.Country(None, "Cancel")
  selection = countryLines[selectionIndex]

  for c in countries:
    if c.name == selection:
      return c

  return country.Country(None, "Disable")

