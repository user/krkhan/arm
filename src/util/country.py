"""
Helper functions for countries parsing.
"""

import os
import sys

from util import torTools

COUNTRIES_FILE = "resources/countries.txt"

class Country:
  def __init__(self, code, name):
    self.code = code
    self.name = name

def get_exit_countries():
  countries = read_countries()

  conn = torTools.getConn()

  relayAddressMappings = conn.getAllRelayAddresses()

  if relayAddressMappings:
    exitIps = []
    for ipAddress in relayAddressMappings:
      for _, fingerprint in relayAddressMappings[ipAddress]:
        if ipAddress in exitIps: continue
        descriptor = conn.getDescriptorEntry(fingerprint)
        if descriptor:
          exitPolicy, exitPolicyEntries = None, []

          for line in descriptor.split("\n"):
            if line.startswith("accept ") or line.startswith("reject "):
              exitPolicyEntries.append(line)

          exitPolicyEntries.reverse()

          for entry in exitPolicyEntries:
            exitPolicy = torTools.ExitPolicy(entry, exitPolicy)

          if exitPolicy and exitPolicy.isExitingAllowed():
            exitIps.append(ipAddress)

    exitCodes = set()
    for ipAddress in exitIps:
      exitCodes.add(conn.getInfo("ip-to-country/%s" % ipAddress, "??"))

    exitCountries = []
    for exitCode in sorted(exitCodes):
      if countries.has_key(exitCode):
        exitCountries.append(Country(exitCode, countries[exitCode]))

    return exitCountries

def read_countries():
  pathPrefix = os.path.dirname(sys.argv[0])
  if pathPrefix and not pathPrefix.endswith('/'):
    pathPrefix = pathPrefix + '/'

  countriesFile = open('%s%s' % (pathPrefix, COUNTRIES_FILE), 'r')
  countries = {}
  for line in countriesFile.readlines():
    code, name = line.strip().split('\t')
    countries[code] = name
    # countries.append(Country(*line.strip().split('\t')))
  countriesFile.close()

  return countries

